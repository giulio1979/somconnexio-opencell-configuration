# SomConnexio OpenCell Configuration

Project with Postman collection used to configure the OpenCell instance of Som Connexió

This project uses the API exposed by OpenCell: https://api.opencellsoft.com/

We are currently running on OpenCell 7.3.0. You can find the endpoints at https://api.opencellsoft.com/7.3.0/

## Setup

1. Download and install the Postman client: https://www.postman.com/downloads/

> We don't know a PPA to install Postman via package manager. Please look this thread for more information: https://github.com/postmanlabs/postman-app-support/issues/2931

2. Import the collection from a link: https://gitlab.com/coopdevs/somconnexio-opencell-configuration/-/raw/master/SC_Config.postman_collection.json
3. Import the environment from Bitwarden called `OpenCell Staging.postman_environment.json`
4. You are now ready to execute the calls!

## Create new requests

1. Create new request.
2. Set the URL with the environment variables. Ex: `{{opencell.url}}/invoice/sendByEmail`
3. Set the HTTP method.
4. Set the Authorization config:

* Type: Basic Auth
* Username: `{{opencell.username}}`
* Password: `{{opencell.password}}`

5. Set the Headers:

* One Authorization header was added with the authorization configuration
* Content-Type: application/json

6. Fill the Body or the Params.
7. Send the request.

## How add a new request to the Collection

Please, look the wiki page defining the flow to add/update the collection:

https://gitlab.com/coopdevs/somconnexio-opencell-configuration/-/wikis/Fer-un-canvi-al-Postam-Collection

(Only in catalan.)
